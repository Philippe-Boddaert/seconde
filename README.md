# Cours SNT

Ce dépôt contient les supports de l'enseignement de __2nde SNT__.

## Découpage

Un chapitre contient des éléments introductifs à l'enseignement de SNT :

<table cellspacing="0" cellpadding="0" style="border-collapse:collapse; margin:auto;">
<tr><td colspan=6 style="text-align: center;"><a title="Introduction" href="./introduction"><img src='./assets/intro.svg' width="128px"/><br/>Introduction</a></td></tr>
</table>

L'enseignement de SNT se décompose en 8 chapitres :

<table cellspacing="0" cellpadding="0" style="border-collapse: collapse;margin:auto;" >
<body>
<tr>
<td style="text-align: center;"><a title="Informatique embarquée" href="./informatique_embarquee"><img src='./assets/microship.svg' width="128px"/><br/>Informatique embarquée</a></td>
<td style="border: none;text-align: center;"><a title="Internet" href="./internet"><img src='./assets/internet.svg' width="128px"/><br/>Internet</a></td>
<td style="border: none;text-align: center;"><a title="Web" href="./web"><img src='./assets/web.svg' width="128px"/><br/>Le Web</a></td>
<td style="border: none;text-align: center;"><a title="Réseaux sociaux" href="./reseaux_sociaux"><img src='./assets/social.svg' width="128px"/><br/>Les réseaux sociaux</a></td>
<td style="border: none;text-align: center;"><a title="Localisation" href="./localisation"><img src='./assets/localisation.svg' width="128px"/><br/>Localisation</a></td>
<td style="border: none;text-align: center;"><a title="La photographie numérique" href="./photographie"><img src='./assets/photo.svg' width="128px"/><br/>La photographie numérique</a></td>
<td style="border: none;text-align: center;"><a title="Les données structurées" href="./donnees_structurees"><img src='./assets/data.svg' width="128px"/><br/>Les données structurées</a></td>
<td style="border: none;text-align: center;"><a title="Programmation" href="./programmation"><img src='./assets/python.svg' width="128px"/><br/>Programmation</a></td>
</tr>
</body>
</table>


## Progression

| Trimestre | Séquence 1                                      | Séquence 2                                |
| --------- | ----------------------------------------------- | ----------------------------------------- |
| 1         | <a title="Introduction" href="./introduction"><br/>Introduction</a> <br /><a title="Internet" href="./internet"><br/>Internet</a>                   | <a title="Programmation" href="./programmation">Programmation</a>                             |
| 2         | <a title="Web" href="./web">Le Web</a>                                         | <a title="Réseaux sociaux" href="./reseaux_sociaux">Les réseaux sociaux</a><br /><a title="Les données structurées" href="./donnees_structurees">Les données structurées</a> |
| 3         | <a title="Localisation" href="./localisation">Localisation</a><br /><a title="Informatique embarquée" href="./informatique_embarquee">Informatique embarquée</a> | <a title="La photographie numérique" href="./photographie">La photographie numérique</a>            |

## Évaluation

L'évaluation de SNT se fait tout au long de l'année à travers différents modes : [Exposés](./exposes), travaux en groupes, mini-projets, QCM...
